﻿using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Running;

namespace Log;

public class Benchmarks {
#if RELEASE
    [Fact]
#else
    [Fact(Skip = "To run the benchmark, build in Release config.")]
#endif
    public void LogIndex() => BenchmarkRunner.Run<LogIndexBenchmarks>();
}

[MemoryDiagnoser]
public class LogIndexBenchmarks : IDisposable {
    readonly LogIndex _index;

    /// <summary>
    /// Initializes a new <see cref="LogIndexBenchmarks"/>.
    /// </summary>
    public LogIndexBenchmarks() {
        _index = new LogIndex();
    }

    /// <summary>
    /// Benchmarks advancing the index for a single payload.
    /// </summary>
    [Benchmark]
    public void Advance() {
        _index.Advance(0x3c00); // simulate 15k payload
        _index.Truncate();
    }

    /// <summary>
    /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
    /// </summary>
    public void Dispose() => _index.Dispose();
}