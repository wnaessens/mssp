﻿namespace Log.Extensions;

/// <summary>
/// Utility class to subscribe to and unsubscribe from events by leveraging <see cref="IDisposable"/>.
/// </summary>
/// <typeparam name="TEventHandler">The type of the event handler.</typeparam>
class EventSubscription<TEventHandler> : IDisposable {
    readonly Action<TEventHandler> _removeHandler;
    readonly TEventHandler _handler;

    /// <summary>
    /// Initializes a new <see cref="EventSubscription{TEventHandler}"/>.
    /// </summary>
    /// <param name="addHandler">An <see cref="Action"/> that attaches a <typeparamref name="TEventHandler"/> to an event.</param>
    /// <param name="removeHandler">An <see cref="Action"/> that detaches a <typeparamref name="TEventHandler"/> from an event.</param>
    /// <param name="handler">A <see cref="TEventHandler"/> to invoke whenever the event is triggered.</param>
    public EventSubscription(Action<TEventHandler> addHandler, Action<TEventHandler> removeHandler, TEventHandler handler) {
        _removeHandler = removeHandler ?? throw new ArgumentNullException(nameof(removeHandler));
        _handler = handler;
        addHandler(handler);
    }

    /// <summary>
    /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
    /// </summary>
    public void Dispose() =>
      _removeHandler(_handler);
}